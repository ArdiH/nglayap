<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('customer.home.index');
})->name('homee');

Auth::routes();

Route::get('/login/admin', 'Auth\LoginController@showAdminLoginForm');
Route::post('/login/admin', 'Auth\LoginController@loginAdmin');
Route::post('/login', 'Auth\LoginController@login')->name('loginValidation');



Route::namespace('admin')->middleware('auth:web')->group( function () {
    Route::group(['prefix' => 'admin','as' => 'admin.'], function(){
        Route::get('/', 'homeController@index')->name('home');
    });

    Route::group(['prefix' => 'users','as' => 'users.'], function(){
        Route::get('/userProfile', 'usersController@index')->name('userProfile');
        Route::post('/userProfile/update/{profiles}', 'usersController@update')->name('updateProfile');

        //user account
        Route::get('/userAccount', 'usersController@indexUserAccount')->name('account');
        Route::get('/userAccount/create', 'usersController@addUserAccount')->name('addAccount');
        Route::post('/userAccount/store', 'usersController@storeUserAccount')->name('storeAccount');

        Route::get('/userAccount/edit/{id}', 'usersController@editUserAccount')->name('editAccount');
        Route::post('/userAccount/update/{id}', 'usersController@updateUserAccount')->name('updateAccount');
        Route::delete('/userAccount/delete/{userid}', 'usersController@destroyUserAccount')->name('destroyAccount');
    });

    Route::group(['prefix' => 'wisata','as' => 'wisata.'], function(){
        Route::get('/', 'wisataController@index')->name('index');
        Route::get('/create', 'wisataController@create')->name('create');
        Route::post('/store', 'wisataController@store')->name('store');
        Route::get('/edit/{id}', 'wisataController@edit')->name('edit');
        Route::post('/update/{id}', 'wisataController@update')->name('update');
        Route::delete('/delete/{id}', 'wisataController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'hotel','as' => 'hotel.'], function(){
        Route::get('/', 'hotelController@index')->name('index');
        Route::get('/create', 'hotelController@create')->name('create');
        Route::post('/store', 'hotelController@store')->name('store');
        Route::get('/edit/{id}', 'hotelController@edit')->name('edit');
        Route::post('/update/{id}', 'hotelController@update')->name('update');
        Route::delete('/delete/{id}', 'hotelController@destroy')->name('destroy');

        // kamar hotel
        Route::get('/kamar', 'hotelController@indexKamar')->name('indexKamar');
        Route::get('/kamar/edit/{id}', 'hotelController@editKamar')->name('editKamar');
        Route::get('/kamar/create/{id}', 'hotelController@createKamar')->name('createKamar');
        Route::post('/kamar/store', 'hotelController@storeKamar')->name('storeKamar');
        Route::post('/kamar/update/{id}', 'hotelController@updateKamar')->name('updateKamar');
        Route::delete('/kamar/delete/{id}', 'hotelController@destroyKamar')->name('destroyKamar');
    });

    Route::group(['prefix' => 'kuliner','as' => 'kuliner.'], function(){
        Route::get('/', 'kulinerController@index')->name('index');
        Route::get('/create', 'kulinerController@create')->name('create');
        Route::post('/store', 'kulinerController@store')->name('store');
        Route::get('/edit/{id}', 'kulinerController@edit')->name('edit');
        Route::post('/update/{id}', 'kulinerController@update')->name('update');
        Route::delete('/delete/{id}', 'kulinerController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'perhitungan','as' => 'perhitungan.'], function(){
        // kriteria
        Route::get('/kriteria', 'perhitunganController@indexKriteria')->name('indexKriteria');
        Route::get('/kriteria/create', 'perhitunganController@createKriteria')->name('createKriteria');
        Route::post('/kriteria/store', 'perhitunganController@storeKriteria')->name('storeKriteria');
        Route::get('/kriteria/edit/{id}', 'perhitunganController@editKriteria')->name('editKriteria');
        Route::post('/kriteria/update/{id}', 'perhitunganController@updateKriteria')->name('updateKriteria');
        Route::delete('/kriteria/delete/{id}', 'perhitunganController@destroyKriteria')->name('destroyKriteria');

        // range
        Route::get('/range', 'perhitunganController@indexRange')->name('indexRange');
        Route::get('/range/create', 'perhitunganController@createRange')->name('createRange');
        Route::post('/range/store', 'perhitunganController@storeRange')->name('storeRange');
        Route::get('/range/edit/{id}', 'perhitunganController@editRange')->name('editRange');
        Route::post('range/update/{id}', 'perhitunganController@updateRange')->name('updateRange');
        Route::delete('/range/delete/{id}', 'perhitunganController@destroyRange')->name('destroyRange');

        // Lokasi Awal
        Route::get('/lokasi', 'perhitunganController@indexLokasi')->name('indexLokasi');
        Route::get('/lokasi/create', 'perhitunganController@createLokasi')->name('createLokasi');
        Route::post('/lokasi/store', 'perhitunganController@storeLokasi')->name('storeLokasi');
        Route::get('/lokasi/edit/{id}', 'perhitunganController@editLokasi')->name('editLokasi');
        Route::post('/lokasi/update/{id}', 'perhitunganController@updateLokasi')->name('updateLokasi');
        Route::delete('/lokasi/delete/{id}', 'perhitunganController@destroyLokasi')->name('destroyLokasi');

        // data prediksi
        Route::get('/dataPrediksi', 'perhitunganController@indexDataPrediksi')->name('indexDataPrediksi');
        Route::get('/dataPrediksi/create', 'perhitunganController@createDataPrediksi')->name('createDataPrediksi');
        Route::get('/getData', 'perhitunganController@getDataPrediksi')->name('getDataPrediksi');
        Route::post('/dataPrediksi/store', 'perhitunganController@storeDataPrediksi')->name('storeDataPrediksi');
        Route::get('/dataPrediksi/edit/{id}', 'perhitunganController@editDataPrediksi')->name('editDataPrediksi');
        Route::post('/dataPrediksi/update/{id}', 'perhitunganController@updateDataPrediksi')->name('updateDataPrediksi');
        Route::delete('/dataPrediksi/delete/{id}', 'perhitunganController@destroyDataPrediksi')->name('destroyDataPrediksi');
        

    });

    Route::group(['prefix' => 'touring','as' => 'touring.'], function(){
        Route::get('/', 'touringController@index')->name('index');
        Route::get('/detailWisata/{id}', 'touringController@detailWisata')->name('detailWisata');
        Route::get('/detailHotel/{id}', 'touringController@detailHotel')->name('detailHotel');
        Route::get('/detailKuliner/{id}', 'touringController@detailKuliner')->name('detailKuliner');
        Route::get('/formTour', 'touringController@formTour')->name('formTour');
        Route::get('/startRoute', 'touringController@dostartroute')->name('dostartroute');
        Route::get('/startTouring', 'touringController@startTouring')->name('startTouring');
        // Route::get('/edit/{id}', 'kulinerController@edit')->name('edit');
        // Route::post('/update/{id}', 'kulinerController@update')->name('update');
        // Route::delete('/delete/{id}', 'kulinerController@destroy')->name('destroy');
    });

    Route::group(['prefix' => 'pesanan', 'as' => 'pesanan.'], function(){
        Route::get('/', 'pesananController@index')->name('index');

    });

});


Route::namespace('customer')->group( function () {
    Route::group(['prefix' => 'home','as' => 'home.'], function(){
        Route::get('/', 'homeController@index')->name('index');
    });

    Route::group(['prefix' => 'trip', 'as' => 'trip.'], function(){
        Route::get('/', 'tripController@index')->name('index');
        Route::get('/detailWisata/{id}', 'tripController@detailWisata')->name('detailWisata');
        Route::get('/detailKuliner/{id}', 'tripController@detailKuliner')->name('detailKuliner');
        Route::get('/detailHotel/{id}', 'tripController@detailHotel')->name('detailHotel');
        Route::get('/formTour', 'tripController@formTour')->name('formTour');
        Route::get('/startRoute', 'tripController@dostartroute')->name('dostartroute');
        Route::get('/startTouring', 'tripController@startTouring')->name('startTouring');
    });

    Route::group(['prefix' => 'service', 'as' => 'service.'], function(){
        Route::get('/', 'serviceController@index')->name('index');
    });

    Route::group(['prefix' => 'cart', 'as' => 'cart.'], function(){
        Route::get('/', 'cartController@index')->name('index');
        Route::post('/addCart', 'cartController@create')->name('create');
        Route::post('/updateCart/{id}', 'cartController@update')->name('update');
        Route::delete('/delete/{cartid}', 'cartController@destroy')->name('destroy');
    });
       

    Route::group(['prefix' => 'profile', 'as' => 'profile.'], function(){
        Route::get('/', 'profileController@index')->name('index');
        Route::get('/getRegency', 'profileController@getRegency')->name('getRegency');
        Route::post('/update/{id}', 'profileController@update')->name('update');
    });

    

});

