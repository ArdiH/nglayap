@extends('admin.layouts.app')
@section('extrahead')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection

@section('content')
@section('pesanan', 'active')
	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>Pesanan</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
							<li class="breadcrumb-item active">Pesanan</li>
						</ol>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-12">
					@if (session('status'))
						<div class="col-md-12">
							<div class="card bg-gradient-success">
								<div class="card-header">
									<h3 class="card-title">Success</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
									{{ session('status')}}
								</div>
							</div>
						</div>
					@endif
					@if(session('danger'))
						<div class="col-md-12">
							<div class="card bg-gradient-danger">
								<div class="card-header">
									<h3 class="card-title">Peringatan</h3>
									<div class="card-tools">
										<button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i>
										</button>
									</div>
								</div>
								<div class="card-body">
								{{ session('danger')}}
								</div>
							</div>
						</div>
					@endif
					<div class="card">
						<div class="card-header">
							<a href="{{ route('wisata.create') }}" type="button" class="btn btn-info float-right"><i class="fas fa-plus"></i>Add Pesanan</a>
						</div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama Customer</th>
                                        <th>Tanggal Pemesanan</th>
										<th>Total Pemesanan</th>
										<th>Status Pembayaran</th>
										<th>Action</th>	
									</tr>
								</thead>
								<tbody>
								
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
                                            <td></td>
											<td>
												
											</td>
										</tr>
								
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection

@section('extrascript')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
		$(function () {
			$("#example1").DataTable();
			$('#example2').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": false,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			});
		});
    </script>
@endsection
