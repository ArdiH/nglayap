@extends('admin.layouts.app')
@section('content')
@section('perhitungan','active')
@section('range', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Add Range</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('perhitungan.indexRange') }}">Range</a></li>
                            <li class="breadcrumb-item active">Add Range</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                            </div>
                            <form role="form" action="{{ route('perhitungan.updateRange', $edit->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Kriteria</label>
                                        <select class="custom-select" name="idKriteria">
                                            <option>Pilih Kriteria</option>
                                            @foreach($kriteria as $kriterias)
                                                <option {{$edit->idKriteria == $kriterias->id  ? 'selected' : ''}} value="{{ $kriterias->id }}">{{ $kriterias->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Nama Range</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Range" name="nama" value="{{ $edit->nama }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Rentang (Sertakan Dengan Satuan Jika Ada)</label>
                                        <input type="number" class="form-control" id="name" placeholder="Masukkan Rentangan" name="rentang" value="{{ $edit->rentang }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Bobot</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Bobot" name="bobot" value="{{ $edit->bobot }}">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
    <script>
        $(function () {
            $('.textarea').summernote()
        })
    </script>
@endsection

