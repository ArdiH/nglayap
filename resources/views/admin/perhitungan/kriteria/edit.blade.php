@extends('admin.layouts.app')
@section('content')
@section('perhitungan','active')
@section('kriteria', 'active')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Edit Kriteria</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{ route('perhitungan.indexKriteria') }}">Kriteria</a></li>
                            <li class="breadcrumb-item active">Edit Kriteria</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                            </div>
                            @foreach($edit as $edit)
                            <form role="form" action="{{ route('perhitungan.updateKriteria', $edit->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="name">Nama</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Nama Kriteria" name="nama" value="{{ $edit->nama }}">
                                    </div>
                                    <div class="form-group">
                                        <label>Kategori</label>
                                        <select class="custom-select" name="kategori">
                                            <option disabled>Pilih Kriteria</option>
                                            <option {{$edit->id  ? 'selected' : ''}} value="{{ $edit->kategori }}">{{ $edit->kategori }}</option>
                                            @if($edit->kategori != 'Benefit')
                                                <option value="Benefit">Benefit</option>
                                            @else
                                                <option value="Cost">Cost</option>
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="name">Satuan</label>
                                        <input type="text" class="form-control" id="name" placeholder="Masukkan Satuan" name="satuan" value="{{ $edit->satuan }}">
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary float-right">Simpan</button>
                                </div>
                            </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@section('extrascript')
    <script>
        $(function () {
            $('.textarea').summernote()
        })
    </script>
@endsection

