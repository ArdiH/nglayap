@extends('admin.layouts.app')
@section('content')
@section('touring', 'active')
	<div class="content-wrapper">
		<section class="content-header">
			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h1>Touring</h1>
					</div>
					<div class="col-sm-6">
						<ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('touring.index') }}">Touring</a></li>
							<li class="breadcrumb-item active">Start Touring</li>
						</ol>
					</div>
				</div>
			</div>
		</section>
		<section class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-12">
						<div class="card">
							<a href="{{ route('touring.formTour') }}" type="button" class="btn btn-info float-right">Start Tour</a>
							<div class="card-header p-2">
								<ul class="nav nav-pills">
									<li class="nav-item"><a class="nav-link active" href="#wisata" data-toggle="tab">Wisata</a></li>
									<li class="nav-item"><a class="nav-link" href="#hotel" data-toggle="tab">Hotel</a></li>
									<li class="nav-item"><a class="nav-link" href="#kuliner" data-toggle="tab">Kuliner</a></li>
								</ul>
							</div>
							<div class="card-body">
								<div class="tab-content">
									<div class="active tab-pane" id="wisata">
										<div class="card card-solid">
											<div class="card-body pb-0">
												<div class="row d-flex align-items-stretch">
												@if($items[0] != null)
												@foreach($items[0] as $wisatas)
													<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
														<div class="card bg-light">
															<div class="card-header text-muted border-bottom-0">
																bintang
															</div>
															<div class="card-body pt-0">
																<div class="row">
																	<div class="col-7">
																		<h2 class="lead"><b>({{ $loop->iteration }}) {{ $wisatas->first()->nama }}</b></h2>
																		<p class="text-muted text-sm"><b>About: </b> {{ $wisatas->first()->deskripsi }} </p>
																		<ul class="ml-4 mb-0 fa-ul text-muted">
																			<li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $wisatas->first()->alamat }}</li>
																		</ul>
																	</div>
																		<div class="col-5 text-center">
																		<img src="{{asset('assets/img/wisata/'.$wisatas->first()->foto)}}" alt="" class="img-thumbnail">
																	</div>
																</div>
															</div>
															<div class="card-footer">
																<div class="text-right">
																	<a href="{{ route('touring.detailWisata', $wisatas->first()->id) }}" class="btn btn-sm btn-primary">
																		<i class="fas fa-eye"></i> View Detail
																	</a>
																</div>
															</div>
														</div>
													</div>
												@endforeach
												@endif
												</div>
											</div>
										</div>
									</div>
									<div class="active tab-pane" id="hotel">
										<div class="card card-solid">
											<div class="card-body pb-0">
												<div class="row d-flex align-items-stretch">
												@if($items[1] != null)
													@foreach($items[1] as $hotels)
														<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
															<div class="card bg-light">
																<div class="card-header text-muted border-bottom-0">
																	bintang
																</div>
																<div class="card-body pt-0">
																	<div class="row">
																		<div class="col-7">
																			<h2 class="lead"><b>({{ $loop->iteration }}) {{ $hotels->first()->nama }}</b></h2>
																			<p class="text-muted text-sm"><b>About: </b> {{ $hotels->first()->deskripsi }} </p>
																			<ul class="ml-4 mb-0 fa-ul text-muted">
																				<li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $hotels->first()->alamat }}</li>
																			</ul>
																		</div>
																			<div class="col-5 text-center">
																			<img src="{{asset('assets/img/hotel/'.$hotels->first()->foto)}}" alt="" class="img-thumbnail">
																		</div>
																	</div>
																</div>
																<div class="card-footer">
																	<div class="text-right">
																		<a href="{{ route('touring.detailHotel', $hotels->first()->id) }}" class="btn btn-sm btn-primary">
																			<i class="fas fa-eye"></i> View Detail
																		</a>
																	</div>
																</div>
															</div>
														</div>
													@endforeach
												@endif
												</div>
											</div>
										</div>
									</div>
									<div class="active tab-pane" id="kuliner">
										<div class="card card-solid">
											<div class="card-body pb-0">
												<div class="row d-flex align-items-stretch">
												@if($items[2] != null)
												@foreach($items[2] as $kuliners)
													<div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
														<div class="card bg-light">
															<div class="card-header text-muted border-bottom-0">
																bintang
															</div>
															<div class="card-body pt-0">
																<div class="row">
																	<div class="col-7">
																		<h2 class="lead"><b>({{ $loop->iteration }}) {{ $kuliners->first()->nama }}</b></h2>
																		<p class="text-muted text-sm"><b>About: </b> {{ $kuliners->first()->deskripsi }} </p>
																		<ul class="ml-4 mb-0 fa-ul text-muted">
																			<li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $kuliners->first()->alamat }}</li>
																		</ul>
																	</div>
																		<div class="col-5 text-center">
																		<img src="{{asset('assets/img/kuliner/'.$kuliners->first()->foto)}}" alt="" class="img-thumbnail">
																	</div>
																</div>
															</div>
															<div class="card-footer">
																<div class="text-right">
																	<a href="{{ route('touring.detailKuliner', $kuliners->first()->id) }}" class="btn btn-sm btn-primary">
																		<i class="fas fa-eye"></i> View Detail
																	</a>
																</div>
															</div>
														</div>
													</div>
												@endforeach
												@endif
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>
@endsection