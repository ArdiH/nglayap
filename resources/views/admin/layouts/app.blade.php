<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="icon" type="image/png" href="{{asset('assets/img/logo/icon.png')}}">
	<title>Admin | NGLAYAP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }} ">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/jqvmap/jqvmap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/dist/css/adminlte.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.css') }}">
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	@yield('extrahead')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">
		<nav class="main-header navbar navbar-expand navbar-white navbar-light">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
				</li>
			</ul>
		</nav>
		<aside class="main-sidebar sidebar-dark-primary elevation-4">
			<a href="index3.html" class="brand-link">
				<img src="{{ asset('assets/img/logo/icon.png') }}" alt="AdminLTE Logo" class="brand-image img-circle">
				<span class="brand-text font-weight-light">NGLAYAP</span>
			</a>
			<div class="sidebar">
				<div class="user-panel mt-3 pb-3 mb-3 d-flex">
					<div class="image">
						<img src="@if(Auth::user()->foto != null) {{asset('assets/img/profile/'.Auth::user()->foto)}} @else {{ asset('assets/dist/img/user2-160x160.jpg') }} @endif" class="img-circle elevation-2" alt="User Image">
					</div>
					<div class="info">
						<a href="#" class="d-block">{{ Auth::user()->name }}</a>
					</div>
				</div>
				<nav class="mt-2">
					<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
						<li class="nav-item">
							<a href="{{ route('admin.home') }}" class="nav-link @yield('dashboard')">
								<i class="nav-icon fas fa-tachometer-alt"></i>
								<p>Dashboard</p>
							</a>
						</li>
						<li class="nav-item has-treeview">
							<a href="#" class="nav-link @yield('users')">
								<i class="nav-icon fas fa-user-circle"></i>
								<p>
									Users
									<i class="fas fa-angle-left right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="{{ route('users.userProfile') }}" class="nav-link @yield('userProfile')">
										<i class="far fa-circle nav-icon"></i>
										<p>User Profile</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('users.account') }}" class="nav-link @yield('userAccount')">
										<i class="far fa-circle nav-icon"></i>
										<p>User Account</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item">
							<a href="{{ route('wisata.index') }}" class="nav-link @yield('wisata')">
								<i class="nav-icon fas fa-edit"></i>
								<p>Wisata</p>
							</a>
						</li>
						<li class="nav-item has-treeview">
							<a href="#" class="nav-link @yield('hotel')">
								<i class="nav-icon fas fa-hotel"></i>
								<p>
									Hotel
									<i class="fas fa-angle-left right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="{{ route('hotel.index') }}" class="nav-link @yield('listHotel')">
										<i class="far fa-circle nav-icon"></i>
										<p>List Hotel</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('hotel.indexKamar') }}" class="nav-link @yield('kamarHotel')">
										<i class="far fa-circle nav-icon"></i>
										<p>Kamar Hotel</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item">
							<a href="{{ route('kuliner.index') }}" class="nav-link @yield('kuliner')">
								<i class="nav-icon fas fa-coffee"></i>
								<p>Kuliner</p>
							</a>
						</li>
						<li class="nav-item has-treeview">
							<a href="#" class="nav-link @yield('perhitungan')">
								<i class="nav-icon fas fa-calculator"></i>
								<p>
									Perhitungan
									<i class="fas fa-angle-left right"></i>
								</p>
							</a>
							<ul class="nav nav-treeview">
								<li class="nav-item">
									<a href="{{ route('perhitungan.indexKriteria') }}" class="nav-link @yield('kriteria')">
										<i class="far fa-circle nav-icon"></i>
										<p>Kriteria</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('perhitungan.indexRange') }}" class="nav-link @yield('range')">
										<i class="far fa-circle nav-icon"></i>
										<p>Range</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('perhitungan.indexLokasi') }}" class="nav-link @yield('lokasiAwal')">
										<i class="far fa-circle nav-icon"></i>
										<p>Lokasi Awal</p>
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ route('perhitungan.indexDataPrediksi') }}" class="nav-link @yield('dataPrediksi')">
										<i class="far fa-circle nav-icon"></i>
										<p>Data Prediksi</p>
									</a>
								</li>
							</ul>
						</li>
						<li class="nav-item">
							<a href="{{ route('touring.index') }}" class="nav-link @yield('touring')">
								<i class="nav-icon fas fa-plane"></i>
								<p>Touring</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{ route('pesanan.index') }}" class="nav-link @yield('pesanan')">
								<i class="nav-icon fas fa-shopping-cart"></i>
								<p>Pesanan</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
								<i class="nav-icon fas fa-power-off"></i>
								<p>Logout</p>
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" class="hidden">
								@csrf
							</form>
						</li>
					</ul>
				</nav>
			</div>
		</aside>
		@yield('content')
		<footer class="main-footer">
			<div class="float-right d-none d-sm-block">
				<b>Version</b> 1.0.0
			</div>
			<strong><a href="#">NGLAYAP</a>.</strong>Make Easily Trip
		</footer>
		<aside class="control-sidebar control-sidebar-dark"></aside>
	</div>
	<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
	<script>
	$.widget.bridge('uibutton', $.ui.button)
	</script>
	<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/chart.js/Chart.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/sparklines/sparkline.js') }}"></script>
	<script src="{{ asset('assets/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
	<script src="{{ asset('assets/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
	<script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
	<script src="{{ asset('assets/dist/js/adminlte.js') }}"></script>
	<script src="{{ asset('assets/dist/js/pages/dashboard.js') }}"></script>
	<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
	@yield('extrascript')
	</body>
</html>
