@extends('layouts.app')
@section('content')
@section('trip', 'active')
	 <!--================Home Banner Area =================-->
    <section class="home_banner_area" id="trip">
        <div class="banner_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4"></div>
                    <div class="col-lg-8">
                        <div class="banner_content">
                            <h1>TRAVELING</h1>
                            <p>IS EASY AND FUN</p>
                            <!-- <div class="btn">
                                <div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
                                <div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
                            </div> -->
                        </divc>
                    </div>
                </div>
            </div>
        </div>
    </section><br>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <div class="card card-primary">
                        <div class="card-header">
                            <center><label>Rekomendasi Pariwisata</label></center>
                        </div>
                        <form role="form" action="{{ route('trip.dostartroute') }}" method="GET">
                        @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Masukkan Lokasi Anda Saat Ini</label>
                                    <select class="custom-select" name="lokasiAwal">
                                        <option>Pilih Lokasi</option>
                                        @foreach($lokasi as $lokasi)
                                            <option value="{{ $lokasi->id }}">{{ $lokasi->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Pilih Kategori Tour</label><br>
                                    <label class="checkbox-inline">
                                        <input type="checkbox" value="1" name="kategori1">Wisata 
                                        <input type="checkbox" value="2" name="kategori2">Hotel
                                        <input type="checkbox" value="3" name="kategori3">Kuliner
                                    </label>
                                    <br>
                                </div>
                                <div class="form-group">
                                    <button type="button"  id="add" class="btn btn-primary">Tambah Form</button>
                                    <button type="button"  id="remove" class="btn btn-danger">Hapus Form</button>
                                </div>
                                <div class="expandwist">
                                    <div id="looping1">
                                        <label>Wisatawan</label>
                                        <div class="form-group">
                                            <label>Pilih Rentang Jarak yang Diinginkan</label><br>
                                            <select class="custom-select" name="jarak[]">
                                                <option>Pilih Rentang Jarak</option>
                                                @foreach($range as $ranges)
                                                    @if($ranges->idKriteria == 1 )
                                                        <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} ({{ $ranges->rentang }} km)</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pilih Rentang Harga yang Diinginkan</label><br>
                                            <select class="custom-select" name="rentangHarga[]">
                                                <option>Pilih Rentang Harga</option>
                                                @foreach($range as $ranges)
                                                    @if($ranges->idKriteria == 2 )
                                                        <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} (Rp. {{ $ranges->rentang }})</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Pilih Rentang Rating yang Diinginkan</label><br>
                                            <select class="custom-select" name="rating[]">
                                                <option>Pilih Rentang Rating</option>
                                                @foreach($range as $ranges)
                                                    @if($ranges->idKriteria == 3 )
                                                        <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} ({{ $ranges->rentang }} )</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary float-right">Start Tour</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header p-2">
                            <ul class="nav nav-pills">
                                <li class="nav-item"><a class="nav-link active" href="#wisata" data-toggle="tab">Wisata</a></li>
                                <li class="nav-item"><a class="nav-link" href="#hotel" data-toggle="tab">Hotel</a></li>
                                <li class="nav-item"><a class="nav-link" href="#kuliner" data-toggle="tab">Kuliner</a></li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="active tab-pane" id="wisata">
                                    <div class="card card-solid">
                                        <div class="card-body pb-0">
                                            <div class="row d-flex align-items-stretch">
                                            @if($items[0] != null)
                                            @foreach($items[0] as $wisatas)
                                                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                                    <div class="card bg-light">
                                                        <div class="card-header text-muted border-bottom-0">
                                                            @for ($i = 1; $i <= $wisatas[0]->wisata()->first()->rating; $i++)
                                                                <span class="fa fa-star checked"></span>
                                                            @endfor
                                                        </div>
                                                        <div class="card-body pt-0">
                                                            <div class="row">
                                                                <div class="col-7">
                                                                    <h2 class="lead"><b>({{ $loop->iteration }}) {{ $wisatas[0]->wisata()->first()->nama }}</b></h2>
                                                                    <p class="text-muted text-sm"><b>Harga: </b> {{ $wisatas[0]->wisata()->first()->harga }} </p>
                                                                    <p class="text-muted text-sm"><b>Jarak: </b> {{ $wisatas[0]->jarak }} km </p>
                                                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $wisatas[0]->wisata()->first()->alamat }}</li>
                                                                    </ul>
                                                                </div>
                                                                    <div class="col-5 text-center">
                                                                    <img src="{{asset('assets/img/wisata/'.$wisatas[0]->wisata()->first()->foto)}}" alt="" class="img-thumbnail">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="text-right">
                                                                <a href="{{ route('touring.detailWisata', $wisatas[0]->wisata()->first()->id) }}" class="btn btn-sm btn-primary">
                                                                    <i class="fas fa-eye"></i> View Detail
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="active tab-pane" id="hotel">
                                    <div class="card card-solid">
                                        <div class="card-body pb-0">
                                            <div class="row d-flex align-items-stretch">
                                            @if($items[1] != null)
                                                @foreach($items[1] as $hotels)
                                                    <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                                        <div class="card bg-light">
                                                            <div class="card-header text-muted border-bottom-0">
                                                                @for ($i = 1; $i <= $hotels[0]->hotel()->first()->rating; $i++)
                                                                    <span class="fa fa-star checked"></span>
                                                                @endfor
                                                            </div>
                                                            <div class="card-body pt-0">
                                                                <div class="row">
                                                                    <div class="col-7">
                                                                        <h2 class="lead"><b>({{ $loop->iteration }}) {{ $hotels[0]->hotel()->first()->nama }}</b></h2>
                                                                        <p class="text-muted text-sm"><b>Harga: </b> {{ $hotels[0]->hotel()->first()->harga }} </p>
                                                                        <p class="text-muted text-sm"><b>Jarak: </b> {{ $hotels[0]->jarak }} km </p>
                                                                        <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $hotels[0]->hotel()->first()->alamat }}</li>
                                                                        </ul>
                                                                    </div>
                                                                        <div class="col-5 text-center">
                                                                        <img src="{{asset('assets/img/hotel/'.$hotels[0]->hotel()->first()->foto)}}" alt="" class="img-thumbnail">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="text-right">
                                                                    <a href="{{ route('touring.detailHotel', $hotels[0]->hotel()->first()->id) }}" class="btn btn-sm btn-primary">
                                                                        <i class="fas fa-eye"></i> View Detail
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="active tab-pane" id="kuliner">
                                    <div class="card card-solid">
                                        <div class="card-body pb-0">
                                            <div class="row d-flex align-items-stretch">
                                            @if($items[2] != null)
                                            @foreach($items[2] as $kuliners)
                                                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
                                                    <div class="card bg-light">
                                                        <div class="card-header text-muted border-bottom-0">
                                                            @for ($i = 1; $i <= $kuliners[0]->kuliner()->first()->rating; $i++)
                                                                <span class="fa fa-star checked"></span>
                                                            @endfor
                                                        </div>
                                                        <div class="card-body pt-0">
                                                            <div class="row">
                                                                <div class="col-7">
                                                                    <h2 class="lead"><b>({{ $loop->iteration }}) {{ $kuliners[0]->kuliner()->first()->nama }}</b></h2>
                                                                    <p class="text-muted text-sm"><b>About: </b> {{ $kuliners[0]->kuliner()->first()->harga }} </p>
                                                                    <p class="text-muted text-sm"><b>Jarak: </b> {{ $kuliners[0]->jarak }} km </p>
                                                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Alamat: {{ $kuliners[0]->kuliner()->first()->alamat }}</li>
                                                                    </ul>
                                                                </div>
                                                                    <div class="col-5 text-center">
                                                                    <img src="{{asset('assets/img/kuliner/'.$kuliners[0]->kuliner()->first()->foto)}}" alt="" class="img-thumbnail">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <div class="text-right">
                                                                <a href="{{ route('touring.detailKuliner', $kuliners[0]->kuliner()->first()->id) }}" class="btn btn-sm btn-primary">
                                                                    <i class="fas fa-eye"></i> View Detail
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('extrascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
<script>
	$(document).ready(function(){
        var no = 2;
        if(no == 2){
            $('#remove').hide();
        }
       
        // var counter = 1;
        var jarak = ('<div class="form-group"><label>Pilih Rentang Jarak yang Diinginkan</label><br><select class="custom-select" name="jarak[]"><option>Pilih Rentang Jarak</option> @foreach($range as $ranges) @if($ranges->idKriteria == 1 ) <option value="{{ $ranges->bobot }}">{{ $ranges->nama }} (Rp. {{ $ranges->rentang }})</option> @endif @endforeach</select></div>');
        var harga = ('<div class="form-group"><label>Pilih Rentang Harga yang Diinginkan</label><br><select class="custom-select" name="rentangHarga[]"><option>Pilih Rentang Harga</option>@foreach($range as $ranges)@if($ranges->idKriteria == 2 )<option value="{{ $ranges->bobot }}">{{ $ranges->nama }} (Rp. {{ $ranges->rentang }})</option> @endif @endforeach</select></div>');
        var rating = ('<div class="form-group"><label>Pilih Rentang Rating yang Diinginkan</label><br><select class="custom-select" name="rating[]"><option>Pilih Rentang Rating</option>@foreach($range as $ranges) @if($ranges->idKriteria == 3 )<option value="{{ $ranges->bobot }}">{{ $ranges->nama }} ({{ $ranges->rentang }} )</option> @endif @endforeach</select></div>');

		$("#add").click(function(){
            $(".expandwist").append('<div id="looping'+no+'"><br><br><label>Wisatawan '+no+'</label><br>'+jarak+harga+rating+'</div>');
            $('#remove').show();
            no++
        });

        $("#remove").click(function() {
            if(no>3){
                $('.expandwist').find('#looping'+(no-1)).remove();
                no--;
            }else{
                $('.expandwist').find('#looping'+(no-1)).remove();
                no--;
                $('#remove').hide();
            }
        });

        
	});
</script>
@endsection