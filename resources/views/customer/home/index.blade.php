@extends('customer.layouts.app')
@section('content')
@section('home', 'active')
   <!--================Home Banner Area =================-->
   <section class="home_banner_area" id="home">
            <div class="banner_inner">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-8">
                            <div class="banner_content">
                                <h1>TRAVELING</h1>
                                <p>IS EASY AND FUN</p>
                                <!-- <div class="btn">
                                    <div class="nav-item"><a href="{{ route('login') }}" class="sm-btn primary_btn">sign in</a></div>
                                    <div class="nav-item"><a href="#" class="sm-btn secondary_btn">sign up</a></div>
                                </div> -->
                            </divc>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about_us_area section_gap_top">
            <div class="container">
                <div class="row about_content align-items-center">
                    <div class="col-lg-6">
                        <div class="section_content">
                            <h6>About Us</h6>
                            <h1>We Believe that <br>Interior beauty Lasts Long</h1>
                            <p>inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct standards
                                especially in the workplace. That’s why it’s crucial that as women.</p>
                            <a class="secondary_btn text-primary" href="#">Learn More</a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about_us_image_box justify-content-center">
                            <img class="img-fluid w-100" src=" {{ asset('assets/img/about_img.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection