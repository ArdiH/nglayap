-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 22, 2020 at 02:58 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nglayap`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hotels`
--

CREATE TABLE `hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bintangHotel` int(11) NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `hotels`
--

INSERT INTO `hotels` (`id`, `foto`, `nama`, `bintangHotel`, `harga`, `alamat`, `rating`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'hotel88.jpg', 'Hotel 88', 2, '320226', 'Jalan Diponegoro No. 43. Jember. Kaliwates. Jember.', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'hotelSulawesi.jpg', 'Hotel Sulawesi', 3, '244480', 'Jalan KH A Dahlan 33. Kaliwates. Kaliwates. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'dafam.jpg', 'Hotel Dafam Lotus', 4, '405239', ' Jalan Jendral. Jl. Gatot Subroto No.47. Jember Regency. ', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'asri.jpg', 'Hotel Asri', 1, '146810', 'Jalan Gatot Subroto No.39. Kaliwates. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'merdeka.jpg', 'Hotel Merdeka', 0, '248000', 'Jalan Sultan Agung No.116. Kaliwates. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'doho.jpeg', 'Doho Homstay', 3, '235963', 'Doho Homestay. Jl.Doho No.8. Lingkungan Sumberdand. Kebonsari. Sumbersari.', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'gm253.jpg', 'Hotel GM253', 2, '330000', 'Jl. Gajah Mada No.253. Patimura. Jember Kidul. Kec. Kaliwates. Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'aston.jpg', 'Hotel Aston', 4, '421080', 'Jl. Sentot Prawirodirdjo No.88. Telengsah. Kec. Kaliwates. Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'luminor.jpg', 'Hotel Luminor', 3, '388135', 'Jl. KH Agus Salim No.28. Tegal Besar Kulon. Tegal Besar. Kaliwates. Jember ', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'royal.jpg', 'Hotel Royal', 3, '352529', 'Jl. Karimata No. 50 Kav. 2. Gumuk Kerang. Kec. Sumbersari', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'cendrawasih.jpg', 'Hotel Cendrawasih', 0, '99816', 'Jalan Cendrawasih 22. Patrang. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'lestari.jpg', 'Hotel Lestari', 2, '188388', 'Jl. Gajah Mada No.233. Kaliwates Kidul. Kec. Kaliwates', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'mulia.jpg', 'Hotel Bintang Mulia', 3, '352941', 'Nusantara Street No. 18. Kaliwates Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'innbox.jpg', 'Innbox Capsule Hotel', 2, '150000', 'Jalan Kalimantan 4 No. 8 Sumbersari. Sumbersari. Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'ebiz.jpg', 'Hotel Ebis', 2, '146558', 'Jalan Kalimantan No.4. Sumbersari. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'sevenDream.jpg', 'Hotel Seven Dream Syariah', 2, '280000', 'Jalan Riau No.2. Kecamatan Sumbersari. Sumbersari. Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'cempaka.jpg', 'Hotel Cempaka Hill', 3, '168646', 'Jl. Cempaka No. 50. Patrang. Patrang. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'arowana.jpg', 'Hotel Arowana', 3, '160000', 'Jalan Arowana 71. Panti. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'bandungPermai.jpg', 'Hotel Bandung Permai', 3, '215824', 'Jalan Hayam Wuruk No.38. Panti. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'greenHill.jpg', 'Hotel GreenHill', 3, '165333', 'Jalan Raya Rembangan No. 99 Baratan Jember. Patrang', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'ambulu.jpg', 'Hotel Ambulu', 3, '130000', 'Jalan Manggar No. 200. Ambulu. Wuluhan. Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'peak.jpg', 'The Peak Guest House', 2, '140700', 'Jalan Perumahan Gunung Batu H - 16 RT 03. RW 07 Kelurahan Sumbersari. Kecamatan Sumbersari. Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'rumahKita.jpg', 'Rumah Kita Jember', 0, '199000', 'Jl. Srikoyo No.25, Krajan, Patrang, Kec. Patrang, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'mitra.jpg', 'Mitra Guest House', 3, '79862', 'EH-6, Jalan Majapahit, Kaliwates Kidul, Kaliwates, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'safari.jpg', 'Hotel Safari', 2, '254343', 'Kebondalem, Jl. KH Achmad Dahlan No.33, Kebondalem, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'ria.jpg', 'Ria Hotel Jember', 3, '140000', 'Jl. Untung Suropati, Tembaan, Kepatihan, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'kertanegara.jpg', 'Kertanegara Homestay', 2, '346500', 'Jl. Kerta Negara, Patimura, Jember Kidul, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'airy.jpeg', 'Airy Kaliwates', 1, '164603', 'Jl. Gajah Mada No.233, Kaliwates Kidul, Kaliwates, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'reddoorz.jpg', 'RedDoorz Syariah', 1, '99000', 'Unnamed Road, Krajan, Sempusari, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'reddoorz.jpg', 'RedDoorz near Lippo Plaza', 2, '140813', 'Jl. Sunan Drajat No.31, Telengsah, Jember Kidul, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'oemah.jpg', 'Oemah Jawa Family Residence', 2, '239239', '3, Jl. Jawa No.3, Tegal Boto Lor, Sumbersari, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'alice.jpeg', 'Alice Homestay Syariah', 2, '164471', 'Jl. Citarum, Kp. Using, Jemberlor, Kec. Patrang, Kabupaten Jember, Jawa Timur', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'residence.jpg', 'Residence Syariah', 2, '115599', 'A, Kab. Kidul, Jl. Melati Gg. Buntu No.176, Kb. Kidul, Jember Kidul, Kec. Kaliwates, Kabupaten Jember, Jawa Timur', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'foresta.jpg', 'Foresta Papuma', 0, '445574', 'Jl. Raya, Area Kebun, Lojejer, Wuluhan, Jember Regency, East Java', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'utama.jpg', 'Utama Kost Harian', 3, '185000', 'Jl. Sumatra 120 A, Kost Harian Jember, Penginapan Jember, Hotel Jember, Tegal Boto Lor, Sumbersari, Kec. Sumbersari', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'antosa.jpg', 'Antosa Family Residence', 2, '107990', 'Jl. Rotawu No.192, Gumuk Kerang, Sumbersari, Kec. Sumbersari, Kabupaten Jember,', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'airlangga.jpg', 'Airlangga Homestay', 0, '300000', 'Perum Kodim lll/28 Dusun Krajan Desa Jebung, Kecamatan Sukorambi, Karang Miuwo, Mangli, Jember, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'casablanca.jpg', 'Casablanca Homestay Syariah', 0, '350000', 'Griya Casablanca, Cluster d\'Safira, Jl. Letjend Suprapto XIV No.3, Lingkungan Krajan, Kebonsari, Kec. Sumbersari', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_wisatas`
--

CREATE TABLE `jenis_wisatas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_wisatas`
--

INSERT INTO `jenis_wisatas` (`id`, `jenis`, `created_at`, `updated_at`) VALUES
(1, 'Pantai', NULL, NULL),
(2, 'Air Terjun', NULL, NULL),
(3, 'Agrowisata', NULL, NULL),
(4, 'Pemandian', NULL, NULL),
(5, 'Budaya', NULL, NULL),
(6, 'Adventure', NULL, NULL),
(7, 'Religi', NULL, NULL),
(8, 'Buatan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kamar_hotels`
--

CREATE TABLE `kamar_hotels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idHotel` int(11) NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `fasilitas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kamar_hotels`
--

INSERT INTO `kamar_hotels` (`id`, `idHotel`, `foto`, `nama`, `harga`, `fasilitas`, `deskripsi`) VALUES
(1, 1, 'deluxe.jpg', 'Deluxe Room', 360000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet WiFi\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang modern minimalis demi kenyamanan Anda selama menginap di Hotel 88 Diponegoro, Tidak Termasuk Breakfast'),
(2, 1, 'junior.jpg', 'Junior Suite room', 500000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet WiFi\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang modern minimalis demi kenyamanan Anda selama menginap di Hotel 88 Diponegoro.'),
(4, 2, 'deluxe.jpg', 'Deluxe Room', 302000, '-AC\r\n-LED / LCD TV dengan saluran channel internasional\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Mini bar\r\n-Area tempat duduk\r\n-Perlengkapan mandi\r\n-Bath\r\n-Toilet', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(5, 3, 'deluxTwinBeds.jpg', 'Deluxe Twin Beds Room Only', 449090, '-AC\r\n-LED / LCD TV dengan saluran channel internasional\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Mini bar\r\n-Area tempat duduk\r\n-Perlengkapan mandi\r\n-Bath\r\n-Toilet', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(6, 3, 'deluxDouble.jpg', 'Deluxe Double Bed Room Only', 449090, '-AC\r\n-LED / LCD TV dengan saluran channel internasional\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Mini bar\r\n-Area tempat duduk\r\n-Perlengkapan mandi\r\n-Bath\r\n-Toilet', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(7, 3, 'deluxTwin.jpg', 'Deluxe Twin', 579090, '-Tempat tidur yang nyaman\r\n-Tv\r\n-Akses internet wifi\r\n-Ac\r\n-Bath\r\n-Toilet\r\n-Non Smoking Room\r\n-Internet Access', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dari tamu'),
(8, 4, 'standard.jpg', 'Standard Room', 235294, '-Tempat tidur yang nyaman\r\n-TV\r\n-Bath\r\n-Toilet', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(9, 4, 'superior.jpg', 'Superior Room', 294118, '-Tempat tidur yang nyaman\r\n-TV\r\n-Bath\r\n-Toilet', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(10, 4, 'deluxe.jpg', 'Deluxe Room', 340557, '-Tempat tidur yang nyaman\r\n-TV\r\n-Bath\r\n-Toilet', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(11, 5, 'deluxe.jpg', 'DELUXE', 220000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(12, 5, 'junior.jpg', 'JUNIOR SUITE', 244000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access\r\n-Air panas dan dingin', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(13, 5, 'suite.jpg', 'SUITE ROOM', 264000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access\r\n-Air panas dan dingin', 'Kami telah merancang dengan desain yang mewah untuk memenuhi berbagai kebutuhan dan anggaran dari tamu kami. Dilengkapi dengan fasilitas demi kenyamanan anda.'),
(14, 6, 'deluxe.jpg', 'Deluxe Room', 310000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang demi kenyamanan Anda selama menginap. Dilengkapi fasilitas seperti AC, TV dan lain-lain.'),
(15, 6, 'deluxExecutive.jpg', 'Deluxe Executive Room', 350000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang demi kenyamanan Anda selama menginap. Dilengkapi fasilitas seperti AC, TV dan lain-lain.'),
(16, 6, 'platinum.jpg', 'Platinum Room', 442000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang demi kenyamanan Anda selama menginap. Dilengkapi fasilitas seperti AC, TV dan lain-lain.'),
(17, 8, 'superior.jpg', 'Superior Room Only', 421080, '-Jam Alarm\r\n-AC\r\n-TV layar datar\r\n-Akses internet WIFI\r\n-Kamar mandi pribadi\r\n-Fasilitas kamar mandi\r\n-Meja\r\n-Bath\r\n-Toilet\r\n-Non Smoking Room\r\n-Internet Access', 'Setiap kamar di Hotel Aston dilengkapi dengan desain kontemporer modern yang menawarkan kenyamanan dan juga dilengkapi dengan TV LCD dengan  berbagai saluran.'),
(18, 8, 'superior.jpg', 'Superior Room', 493680, '-Jam Alarm\r\n-AC\r\n-TV layar datar\r\n-Akses internet WIFI\r\n-Kamar mandi pribadi\r\n-Fasilitas kamar mandi\r\n-Meja\r\n-Bath\r\n-Toilet\r\n-Non Smoking Room\r\n-Internet Access', 'Setiap kamar di Hotel Aston dilengkapi dengan desain kontemporer modern yang menawarkan kenyamanan dan juga dilengkapi dengan TV LCD dengan  berbagai saluran.'),
(19, 8, 'deluxe.jpg', 'Deluxe Room Only', 542080, '-Jam Alarm\r\n-AC\r\n-TV layar datar\r\n-Akses internet WIFI\r\n-Kamar mandi pribadi\r\n-Fasilitas kamar mandi\r\n-Meja\r\n-Bath\r\n-Toilet\r\n-Non Smoking Room\r\n-Internet Access', 'Setiap kamar di Hotel Aston dilengkapi dengan desain kontemporer modern yang menawarkan kenyamanan dan juga dilengkapi dengan TV LCD dengan  berbagai saluran.'),
(20, 8, 'deluxe.jpg', 'Deluxe Room', 614680, '-Jam Alarm\r\n-AC\r\n-TV layar datar\r\n-Akses internet WIFI\r\n-Kamar mandi pribadi\r\n-Fasilitas kamar mandi\r\n-Meja\r\n-Bath\r\n-Toilet\r\n-Non Smoking Room\r\n-Internet Access', 'Setiap kamar di Hotel Aston dilengkapi dengan desain kontemporer modern yang menawarkan kenyamanan dan juga dilengkapi dengan TV LCD dengan  berbagai saluran.'),
(21, 9, 'superior.jpg', 'Superior Room Only', 400000, '-Bath\r\n-Toilet\r\n-Internet Access', 'a'),
(22, 9, 'superiorBreakfest.jpg', 'Superior Breakfast', 460000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang demi kenyamanan anda selama menginap . Dilengkapi fasilitas seperti AC, TV dan lain-lain.'),
(23, 9, 'deluxeBreakfast.jpg', 'Deluxe Breakfast', 500000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang demi kenyamanan anda selama menginap . Dilengkapi fasilitas seperti AC, TV dan lain-lain.'),
(24, 9, 'executiveBreakfast.jpg', 'Executive Breakfast', 800000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi\r\n-Akses internet\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Kamar dirancang demi kenyamanan anda selama menginap . Dilengkapi fasilitas seperti AC, TV dan lain-lain.'),
(25, 10, 'gold.jpg', 'Gold Standard Room Only', 353000, '-Kamar Mandi Pribadi\r\n-Kamar bebas rokok\r\n-AC\r\n-Meja\r\n-TV LCD/plasma screen\r\n-Satelit/Kable TV\r\n-WiFi gratis\r\n-Air minum botol gratis\r\n-Bath\r\n-Toilet', 'Royal Jember memiliki total 51 kamar Gold Standard yang tersedia untuk pemesanan. Semua kamar di hotel dilengkapi dengan kamar mandi pribadi dengan air panas & dingin, AC, TV dengan channel internasional dan domestic, telepon, dan kamar dihiasi dengan gaya tropis.'),
(26, 10, 'platinumSuperior.jpg', 'Platinum Superior', 507000, '-Kamar Mandi Pribadi\r\n-Kamar bebas rokok\r\n-AC\r\n-Meja\r\n-TV LCD/plasma screen\r\n-Satelit/Kable TV\r\n-WiFi gratis\r\n-Air minum botol gratis\r\n-Bath\r\n-Toilet', 'Royal Jember memiliki total 14 kamar Platinum Superior yang tersedia untuk pemesanan. Semua kamar di hotel dilengkapi dengan kamar mandi pribadi dengan air panas & dingin, AC, TV dengan channel internasional dan domestic, telepon, dan kamar dihiasi dengan gaya tropis.'),
(27, 10, 'shappire.jpg', 'Sapphire Deluxe', 636000, '-Kamar Mandi Pribadi\r\n-Kamar bebas rokok\r\n-AC\r\n-Meja\r\n-TV LCD/plasma screen\r\n-Satelit/Kable TV\r\n-WiFi gratis\r\n-Air minum \r\n-botol gratis\r\n-Bath\r\n-Bathtub\r\n-Toilet', 'Semua kamar di hotel dilengkapi dengan kamar mandi pribadi dengan air panas & dingin, AC, TV dengan channel internasional dan domestic, telepon, dan kamar dihiasi dengan gaya tropis.'),
(28, 11, 'standard.jpg', 'Standart Room', 100000, '-Fan\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi pribadi\r\n-Akses internet\r\n-Perabotan berkualitas\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Luas kamar 17.5 meter persegi, kamar dirancang demi kenyamanan anda selama menginap dihotel ini. Dilengkapi fasilitas seperti Fan, TV, akses internet dan lain-lain.'),
(29, 11, 'deluxe.jpg', 'Deluxe Room', 200000, '-AC\r\n-TV\r\n-Tempat tidur yang nyaman\r\n-Kamar mandi pribadi\r\n-Akses internet\r\n-Perabotan berkualitas\r\n-Bath\r\n-Toilet\r\n-Internet Access', 'Luas kamar 17.5 meter persegi, kamar dirancang demi kenyamanan anda selama menginap dihotel ini. Dilengkapi fasilitas seperti AC, TV, akses internet dan lain-lain.'),
(30, 11, 'superiorA.jpg', 'Superior A', 250000, '-Maks. 2 orang', 'a'),
(31, 13, 'superior.jpg', 'Superior Room', 352941, '-Maks. 2 orang', 'a'),
(32, 13, 'mulia.jpg', 'Mulia Suite Room', 647059, '-Maks. 2 orang', 'a'),
(33, 15, 'standardTwin.jpg', 'Standard Twin Room', 209874, '-Maks. 2 orang', 'a'),
(34, 15, 'standardDouble.jpg', 'Standard Double Room', 215120, '-Maks. 2 orang', 'a'),
(35, 15, 'deluxTwin.jpg', 'Deluxe Twin Room', 251549, '-Maks. 2 orang', 'a'),
(36, 15, 'deluxDouble.jpg', 'Deluxe Double Room', 254684, '-Maks. 2 orang', 'a'),
(37, 15, 'suiteDouble.jpg', 'Suite Double Room', 352169, '-Maks. 2 orang', 'a'),
(38, 15, 'suiteFamily.jpg', 'Suite Family Room', 530593, '-Maks. 2 orang', 'a'),
(39, 16, 'rose.jpg', 'Rose Room', 280000, '-Maks. 2 orang', 'a'),
(40, 16, 'orchid.jpg', 'Orchid Room', 300000, '-Maks. 2 orang', 'a'),
(41, 16, 'jasmine.jpg', 'Jasmine Room', 340000, '-Maks. 2 orang', 'a'),
(42, 17, 'standardDouble.jpg', 'Standard Twin Room', 214098, '-Maks. 2 orang', 'a'),
(43, 17, 'standardDouble.jpg', 'Standard Double Room', 214542, '-Maks. 2 orang', 'a'),
(44, 17, 'deluxTwin.jpg', 'Deluxe Twin Room', 283735, '-Maks. 2 orang', 'a'),
(45, 17, 'deluxDouble.jpg', 'Deluxe Double Room', 283735, '-Maks. 2 orang', 'a'),
(46, 17, 'suiteDouble.jpg', 'Suite Double', 422926, '-Maks. 2 orang', 'a'),
(47, 18, 'standard.jpg', 'Standard Room', 160000, '-Maks. 2 orang', 'a'),
(48, 18, 'vip.jpg', 'VIP Room [GR]', 200000, '-Maks. 2 orang', 'a'),
(49, 18, 'suite.jpg', 'Suite Room [RR]', 200000, '-Maks. 2 orang', 'a'),
(50, 19, 'standardTwin.jpg', 'Standard Twin Room', 249727, '-Maks. 2 orang', 'a'),
(51, 19, 'standardDouble.jpg', 'Standard Double Room', 255971, '-Maks. 2 orang', 'a'),
(52, 19, 'deluxTwin.jpg', 'Deluxe Twin Room', 293429, '-Maks. 2 orang', 'a'),
(53, 19, 'deluxDouble.jpg', 'Deluxe Double Room', 299673, '-Maks. 2 orang', 'a'),
(54, 19, 'suiteDouble.jpg', 'suite double', 399563, '-Maks. 2 orang', 'a'),
(55, 20, 'deluxDouble.jpg', 'Deluxe Double Room', 266642, '-Maks. 2 orang', 'a'),
(56, 20, 'suiteDouble.jpg', 'Suite Double', 332046, '-Maks. 2 orang', 'a'),
(57, 20, 'suiteTwin.jpg', 'Suite Twin', 342107, '-Maks. 2 orang', 'a'),
(58, 20, 'suiteFamily.jpg', 'Suite Family', 723205, '-Maks. 2 orang', 'a'),
(59, 21, 'deluxeFan.jpg', 'Deluxe Fan Room Only', 150000, '-Maks. 2 orang', 'a'),
(60, 21, 'deluxeAc.jpg', 'Deluxe AC', 250000, '-Maks. 2 orang', 'a'),
(61, 21, 'superior.jpg', 'Superior Room', 400000, '-Maks. 2 orang', 'a'),
(62, 21, 'family.jpg', 'Family Suite Room', 450000, '-Maks. 2 orang', 'a'),
(63, 21, 'president.jpg', 'President Suite', 1000000, '-Maks. 2 orang', 'a'),
(64, 22, 'standardDouble.jpg', 'Standard Double Room', 194053, '-Maks. 2 orang', 'a'),
(65, 22, 'deluxDouble.jpg', 'Deluxe Double Room', 194053, '-Maks. 2 orang', 'a'),
(66, 23, 'standard.jpg', 'Standard Room', 199000, '-Maks. 2 orang', 'a'),
(67, 24, 'standard.jpg', 'Standard Room', 80000, '-Maks. 2 orang', 'a'),
(68, 24, 'deluxe.jpg', 'Deluxe Room Only', 105000, '-Maks. 2 orang', 'a'),
(69, 24, 'executive.jpg', 'Executive Room Only', 130000, '-Maks. 2 orang', 'a'),
(70, 25, 'standard.jpg', 'Standard Room', 255000, '-Maks. 2 orang', 'a'),
(71, 25, 'deluxe.jpg', 'Deluxe Room', 289000, '-Maks. 2 orang', 'a'),
(72, 26, 'deluxe.jpg', 'Deluxe room only', 165000, '-Maks. 2 orang', 'a'),
(73, 26, 'deluxeSuite.jpg', 'Deluxe Suite Room Only', 175000, '-Maks. 2 orang', 'a'),
(74, 26, 'executive.jpg', 'Executive Room Only', 190000, '-Maks. 2 orang', 'a'),
(75, 27, 'deluxe.jpg', 'Deluxe Room', 301500, '-Maks. 2 orang', 'a'),
(76, 27, 'executive.jpg', 'Executive Room', 346500, '-Maks. 2 orang', 'a'),
(77, 28, 'standardTwin.jpg', 'Standard Twin Room with Breakfast - Special Promo 7', 164603, '-Maks. 2 orang', 'a'),
(78, 28, 'standardTwin.jpg', 'Standard Twin Room with Breakfast - Special Promo 7', 176993, '-Maks. 2 orang', 'a'),
(79, 28, 'standardDouble.jpg', 'Standard Double Room with Breakfast - Special Promo 7', 164603, '-Maks. 2 orang', 'a'),
(80, 28, 'standardDouble.jpg', 'Standard Double Room with Breakfast - Special Promo 7', 176993, '-Maks. 2 orang', 'a'),
(81, 28, 'superiorTwin.jpg', 'Superior Twin Room with Breakfast - Special Promo 7', 212172, '-Maks. 2 orang', 'a'),
(82, 28, 'superiorDouble.jpg', 'Superior Double Room with Breakfast - Special Promo 7', 212172, '-Maks. 2 orang', 'a'),
(83, 29, 'redDoorzSale.jpg', 'RedDoorz SALE 99K', 99000, '-Maks. 2 orang', 'a'),
(84, 29, 'redDoorzSale.jpg', 'RedDoorz SALE 125K', 125000, '-Maks. 2 orang', 'a'),
(85, 29, 'redDoorzTwin.jpg', 'RedDoorz Twin Room - Basic Deals Promotion', 142500, '-Maks. 2 orang', 'a'),
(86, 29, 'redDoorzTwin.jpg', 'RedDoorz Twin Room', 150000, '-Maks. 2 orang', 'a'),
(87, 29, 'redDoorzRoom.jpg', 'RedDoorz Room - Basic Deals Promotion', 142500, '-Maks. 2 orang', 'a'),
(88, 29, 'redDoorzRoom.jpg', 'RedDoorz Room', 150000, '-Maks. 2 orang', 'a'),
(89, 29, 'redDoorzRoomPremium.jpg', 'RedDoorz Premium Room - Basic Deals Promotion', 161500, '-Maks. 2 orang', 'a'),
(90, 29, 'redDoorzRoomPremium.jpg', 'RedDoorz Premium Room', 170000, '-Maks. 2 orang', 'a'),
(91, 29, 'redDoorzTwin.jpg', 'RedDoorz Twin Room with Breakfast', 210000, '-Maks. 2 orang', 'a'),
(92, 29, 'redDoorzRoom.jpg', 'RedDoorz Room with Breakfast', 210000, '-Maks. 2 orang', 'a'),
(93, 29, 'redDoorzRoomPremium.jpg', 'RedDoorz Premium Room with Breakfast', 230000, '-Maks. 2 orang', 'a'),
(94, 30, 'redDoorzRoom.jpg', 'RedDoorz Room - Basic Deals Promotion', 176700, '-Maks. 2 orang', 'a'),
(95, 30, 'redDoorzRoom.jpg', 'RedDoorz Room', 186000, '-Maks. 2 orang', 'a'),
(96, 30, 'redDoorzRoomPremium.jpg', 'RedDoorz Premium Room - Basic Deals Promotion', 200405, '-Maks. 2 orang', 'a'),
(97, 30, 'redDoorzRoomPremium.jpg', 'RedDoorz Premium Room', 211000, '-Maks. 2 orang', 'a'),
(98, 31, 'standardDouble.jpg', 'Standard Double Room', 144995, '-Maks. 2 orang', 'a'),
(99, 31, 'deluxTwin.jpg', 'Deluxe Twin Room', 166213, '-Maks. 2 orang', 'a'),
(100, 32, 'standardTwin.jpg', 'Standard Twin Room', 182034, '-Maks. 2 orang', 'a'),
(101, 32, 'standardDouble.jpg', 'Standard Double Room', 186585, '-Maks. 2 orang', 'a'),
(102, 33, 'standardDouble.jpg', 'Standard Double Room', 140867, '-Maks. 2 orang', 'a'),
(103, 34, 'villaBigDeal.jpg', 'Villa Jabon Room Only - Big Deal', 550357, '-Maks. 2 orang', 'a'),
(104, 34, 'villaRoom.jpg', 'Villa Jabon Room Only - Special Campaign', 575000, '-Maks. 2 orang', 'a'),
(105, 34, 'villaRoom.jpg', 'Villa Jabon Room Only', 821429, '-Maks. 2 orang', 'a'),
(106, 34, 'villaMahoni.jpg', 'Villa Mahoni Room Only - Big Deal', 550357, '-Maks. 2 orang', 'a'),
(107, 34, 'villaMahoni.jpg', 'Villa Mahoni Room Only - Special Campaign', 575000, '-Maks. 2 orang', 'a'),
(108, 34, 'villaMahoni.jpg', 'Villa Mahoni Room Only', 821429, '-Maks. 2 orang', 'a'),
(109, 34, 'villaRImba.jpg', 'Villa Rimba Room Only - Big Deal', 550357, '-Maks. 2 orang', 'a'),
(110, 34, 'villaRImba.jpg', 'Villa Rimba Room Only - Special Campaign', 575000, '-Maks. 2 orang', 'a'),
(111, 34, 'villaRImba.jpg', 'Villa Rimba Room Only', 821429, '-Maks. 2 orang', 'a'),
(112, 34, 'villaKajang.jpg', 'Villa Kajang Room Only - Big Deal', 646072, '-Maks. 2 orang', 'a'),
(113, 34, 'villaKajang.jpg', 'Villa Kajang Room Only - Special Campaign', 675000, '-Maks. 2 orang', 'a'),
(114, 34, 'villaKajang.jpg', 'Villa Kajang Room Only', 964287, '-Maks. 2 orang', 'a'),
(115, 34, 'villaDjati.jpg', 'Villa Djati Room Only - Big Dea', 646072, '-Maks. 2 orang', 'a'),
(116, 34, 'villaDjati.jpg', 'Villa Djati Room Only - Special Campaign', 675000, '-Maks. 2 orang', 'a'),
(117, 34, 'villaDjati.jpg', 'Villa Djati Room Only', 964287, '-Maks. 2 orang', 'a'),
(118, 34, 'villaSengon.jpg', 'Villa Sengon Room Only - Big Deal', 981071, '-Maks. 2 orang', 'a'),
(119, 34, 'villaSengon.jpg', 'Villa Sengon Room Only - Special Campaign', 1025000, '-Maks. 2 orang', 'a'),
(120, 34, 'villaSengon.jpg', 'Villa Sengon Room Only', 1464286, '-Maks. 2 orang', 'a'),
(121, 35, 'superior.jpg', 'Superior Room', 185000, '-Maks. 2 orang', 'a'),
(122, 35, 'deluxe.jpg', 'Deluxe Room 3 bed', 325000, '-Maks. 2 orang', 'a'),
(123, 35, 'deluxe.jpg', 'Deluxe Room 4 bed', 325000, '-Maks. 2 orang', 'a'),
(124, 36, 'standardDouble.jpg', 'Standard Double Room', 140867, '-Maks. 2 orang', 'a'),
(125, 37, 'superior.jpg', 'Superior Room', 300000, '-Maks. 2 orang', 'a'),
(126, 37, 'family.jpg', 'Family Room', 600000, '-Maks. 2 orang', 'a'),
(127, 38, 'homestay.jpg', 'Homestay 2 Bedroom', 350000, '-Maks. 2 orang', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `kriterias`
--

CREATE TABLE `kriterias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `satuan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kriterias`
--

INSERT INTO `kriterias` (`id`, `nama`, `kategori`, `satuan`) VALUES
(1, 'Jarak', 'Benefit', 'Km'),
(5, 'Harga', 'Cost', 'Rp'),
(6, 'Rating', 'Benefit', '');

-- --------------------------------------------------------

--
-- Table structure for table `kuliners`
--

CREATE TABLE `kuliners` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kuliners`
--

INSERT INTO `kuliners` (`id`, `foto`, `nama`, `harga`, `alamat`, `rating`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'legian.jpg', 'Legian Restauran', '34900', 'Jl. Gajah Mada No.234, Kaliwates, Kec. Kaliwates, Kabupaten Jember,', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'restoPapuma.jpg', 'Resto Papuma', '70000', 'Jl. Sentot Prawirodirdjo No.44A, Telengsah, Jember Kidul, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'sariUtama.jpg', 'Restoran Sari Utama', '50000', 'Jl. Gajah Mada Kelurahan No.27, Kelurahan Jember Kidu, Jember Kidul, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'fujiyama.jpeg', 'Fujiyama Sushi Restaurant', '30000', 'Jl. Gajah Mada No.244, Kaliwates, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'tmi.jpg', 'Restoran Taman Mangli Indah', '35000', 'Jl. Hayam Wuruk No.183, Karang Miuwo, Mangli, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'depot.jpg', 'Depot Jawa Timur', '35000', 'u bb, Jl. Gatot Subroto No.8, Kampungtengah, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'rajaIkan.jpg', 'Rajaikan x kerang kerang seafood', '50000', 'Jl. Manyar No.55, Puring, Slawu, Kec. Patrang, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'darum.jpg', 'Warung bu Darum', '30000', 'Jl. Gajah Mada Kelurahan No.23, Kelurahan Jember Kidu, Jember Kidul, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'martabak.jpg', 'Terang Bulan & Martabak KING', '20000', '50 Sumbersari, Jl. Karimata, Gumuk Kerang, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'gudeg.jpg', 'Gudeg dan Pecel Lumintu', '30000', 'Jl. Kerta Negara Kelurahan No.33, Kelurahan Jember Kidu, Jember Kidul, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'sariJaya.jpg', 'Sari Jaya Restaurant', '30000', 'Jl. Sultan Agung No.124, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'ayam.jpg', 'Ayam Gephok P. Giek', '25000', 'Jalan Mastrip I No. 19A, Sumbersari, Krajan Timur, Sumbersari, Kec. Sumbersari, Kabupaten Jember,', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'anda.jpg', 'Depot Anda Jember', '30000', 'Jl. Gatot Subroto, Tembaan, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'grafity.jpg', 'Graffity Jaya Ayam Goreng Kalasan', '25000', 'Jl. Jawa No.22, Tegal Boto Lor, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'lestari.jpg', 'Lestari Restaurant', '30000', 'Jl. R.A. Kartini No.14 - 16, Kp. Using, Jemberlor, Kec. Patrang, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'kamoelyan.jpg', 'Kamoelyan Grilled Ribs', '20000', 'jl. karimata 34 jember, ( depan hotel Royal ), jember, Sumbersari, Gumuk Kerang, Sumbersari, Kec. Sumbersari', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'kfc.jpg', 'KFC Jember', '78000', 'Jl. Gajah Mada No.74, Kb. Kidul, Jember Kidul, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 'sate.jpeg', 'Warung Sate Pak Toha', '25000', 'Jl. Brawijaya No.31, Karang Miuwo, Mangli, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 'omBoy.jpg', 'Warung Om Boy & Ayam Geprek Tidar', '18000', 'Jl. Tidar, Kloncing, Karangrejo, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 'bebek.jpg', 'Kedai bebek Mbeling', '30000', 'Jl. Hayam Wuruk No.2, Gerdu, Sempusari, Kec. Kaliwates, Kabupaten Jember, Jawa Timur 68131', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 'oma.jpg', 'Dapur Oma', '35000', 'Jl. Gatot Subroto, Tembaan, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 'invi.jpg', 'Invi Food Mastrip', '20000', 'Jl. Mastrip, Krajan Barat, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 'hongdae.jpeg', 'Hongdae roxy', '60000', 'Jl. Hayam Wuruk, Gerdu, Sempusari, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 'bakso.jpg', 'Bakso boejangan', '25000', 'GF 17-18, Jl. Hayam Wuruk No.71, Gerdu, Sempusari, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 'cakSis.jpg', 'Cak Sis Seafood', '70000', 'Jl. R.A. Kartini No.64, Tembaan, Kepatihan, Kec. Kaliwates, Kabupaten Jember, Jawa Timur 68131', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'pecel.jpg', 'Pecel Wali Songo', '25000', 'Jl. Sultan Agung, Kepatihan, Kec. Kaliwates, Kabupaten Jember, Jawa Timur 68131', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 'lenquas.jpg', 'Kedai LenQuas', '25000', 'Jl. Karimata, Gumuk Kerang, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 'soto.jpg', 'Soto Ayu (Pasar Tanjung)', '18000', 'Jl. Samanhudi No.Kelurahan, Kelurahan Jember Kidu, Jember Kidul, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 'mie.jpg', 'Mie Apong Sampoerna', '22000', 'Jl. Diponegoro, Tembaan, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 'gandrung.jpg', 'Mie Gandrung', '22000', 'Jl. Teratai No.55, Gebang Timur, Gebang, Kec. Patrang, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 'clarys.jpg', 'Clarys', '35000', 'Jl. Jendral Ahmad Yani No.43, Kampungtengah, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 'hisana.jpeg', 'Bebek Hisana', '35000', 'Jl. Kalimantan No.27, Krajan Timur, Sumbersari, Kec. Sumbersari', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 'buLilik.jpg', 'Lalapan Bu Lilik Kaliwates', '30000', 'Jl. Gajah Mada, Kaliwates, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 'mcd.jpg', 'McDonalds', '38000', 'Jl. Hayam Wuruk No.40, Gerdu, Sempusari, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 'conato.jpeg', 'Conato Bakery & Cafe', '25000', 'Jalan Gajah Mada, Kaliwates, Kaliwates Kidul, Kaliwates, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 'sampoerna.jpg', 'Bakso dan Mie Ayam Sampoerna', '25000', 'Johar Plaza, Jl. Diponegoro No.3, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 'soponyono.jpg', 'Rumah Makan Soponyono', '13000', 'Jl. Ciliwung No.11, Kp. Using, Jemberlor, Kec. Patrang, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 'kabut.jpg', 'Bakso kabut', '', 'Jl. Rasamala II, Darungan, Kemuning Lor, Arjasa, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 'nelongso.jpg', 'Ayam goreng nelongso', '', 'Jl. Mastrip (Ruko Mastrip Square Blok K, Krajan Barat, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 'martabakPlecongan.jpg', 'Martabak D\'Pecenongan', '', 'Jl. Jendral Ahmad Yani, Kampungtengah, Kepatihan, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 'geprek.jpeg', 'Geprek Bensu', '', 'Jl. Karimata No.58, Gumuk Kerang, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(42, 'calabay.jpg', 'Calabay ', '', 'Tegal Boto Lor, Sumbersari, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 'mieSetan.jpg', 'Mie setan', '', 'Jl. Sumatra No.4, Tegal Boto Lor, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 'vivi.jpg', 'Warung bakaran mbak vivi', '', 'Jl. Nias No.Raya, Tegal Boto Lor, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 'pizza.jpg', 'Pizza hut', '', 'Jl. PB Sudirman No.7, Pagah, Jemberlor, Kec. Patrang, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 'wongSolo.jpg', 'Ayam bakar wong solo', '', 'Jl. Karimata No.7A, Gumuk Kerang, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 'sotoSukri.jpg', 'Soto Ayam H.Syukri', '', 'Jl. Kalimantan No.15A, Krajan Timur, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 'upnormal.jpg', 'Warunk Upnormal', '', 'Jl. Kalimantan No.15A, Krajan Timur, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 'gacoan.jpeg', 'Mie Gacoan', '', 'Jl. Sumatra No.82, Tegal Boto Lor, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 'alula.jpg', 'Geprek Alula', '', 'Jl. Sumatra No.123, Tegal Boto Lor, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `lokasis`
--

CREATE TABLE `lokasis` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lokasis`
--

INSERT INTO `lokasis` (`id`, `nama`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'Stasiun Arjasa', 'Jl. Supriadi, Jawaan, Patemon, Kec. Pakusari, Kabupaten Jember, Jawa Timur ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Stasiun Bangsalsari', 'Kedung Suko, Bangsalsari, Jember Regency, East Java ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Stasiun Jember', 'Jl. Wijaya Kusuma No.5, Tegal Rejo, Jemberlor, Kec. Patrang, Kabupaten Jember, Jawa Timur ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Stasiun Jatiroto', 'Nyeroan, Kaliboto Lor, Jatiroto, Lumajang Regency, East Java 67355', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Stasiun Kalisat', 'Jl. Cokroaminoto, Krajan II, Kalisat, Kabupaten Jember, Jawa Timur 68193', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Stasiun Ledokombo', 'Karang Kebun, Sumber Lesung, Ledokombo, Jember Regency, East Java 68196', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Stasiun Mangli', 'Tanjung, Mangli, Kaliwates, Jember Regency, East Java 68131', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Stasiun Rambipuji', 'Jl. Dharmawangsa, Krajan Lor, Rambigundam, Kec. Rambipuji, Kabupaten Jember, Jawa Timur', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Stasiun Tanggul', 'Tekoan, Tanggul Kulon, Tanggul, Jember Regency, East Java', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Jatiroto', 'Sumberbaru, Jember Regency East Java', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Garahan', 'Krajan, Garahan, Silo, Jember Regency, East Java', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Jelbuk', 'Pakel, Sucopangepok, Jelbuk, Jember Regency, East Java', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Jombang', 'Kecamatan Jombang Jember Regency East Java', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_10_16_122018_create_permission_tables', 1),
(5, '2019_12_26_110347_user_roles', 1),
(6, '2020_01_12_042355_wisata', 1),
(7, '2020_01_13_182951_hotel', 1),
(8, '2020_01_13_223959_kuliner', 1),
(9, '2020_01_16_214703_jenis_wisata', 1),
(10, '2020_01_17_064610_kamar_hotel', 2),
(11, '2020_01_19_100624_kriteria', 3),
(12, '2020_01_20_041732_range', 4),
(13, '2020_01_21_010807_lokasi_awal', 5);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `ranges`
--

CREATE TABLE `ranges` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idKriteria` int(11) NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rentang` int(11) NOT NULL,
  `bobot` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ranges`
--

INSERT INTO `ranges` (`id`, `idKriteria`, `nama`, `rentang`, `bobot`, `created_at`, `updated_at`) VALUES
(1, 1, 'Dekat', 10, 1, '2020-01-20 06:37:30', '2020-01-20 14:35:42'),
(3, 1, 'Menengah', 20, 0.6, '2020-01-20 14:44:59', '2020-01-20 15:38:38'),
(4, 1, 'Jauh', 30, 0.3, '2020-01-20 15:39:15', '2020-01-20 15:39:15'),
(5, 1, 'Sangat Jauh', 100, 0, '2020-01-20 15:39:55', '2020-01-20 15:39:55'),
(6, 5, 'Murah', 20000, 1, '2020-01-20 15:44:07', '2020-01-20 15:44:07'),
(7, 5, 'Sedang', 50000, 0.6, '2020-01-20 16:14:38', '2020-01-20 16:14:38'),
(8, 5, 'Mahal', 100000, 0.3, '2020-01-20 16:15:58', '2020-01-20 16:15:58'),
(9, 5, 'Sangat Mahal', 500000, 0, '2020-01-20 16:16:35', '2020-01-20 16:16:35'),
(10, 6, 'Biasa', 1, 0, '2020-01-20 16:17:18', '2020-01-20 16:17:18'),
(11, 6, 'Sedang', 2, 0.3, '2020-01-20 16:17:40', '2020-01-20 16:17:40'),
(12, 6, 'Populer', 3, 0.6, '2020-01-20 16:17:59', '2020-01-20 16:17:59'),
(13, 6, 'Sangat Populer', 4, 1, '2020-01-20 16:18:18', '2020-01-20 16:18:18');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-01-16 14:56:59', '2020-01-16 14:56:59'),
(2, 'customer', 'web', '2020-01-16 14:56:59', '2020-01-16 14:56:59'),
(3, 'hotels', 'web', '2020-01-16 14:56:59', '2020-01-16 14:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `foto`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Geo Cole', 'admin@admin', '2020-01-16 14:57:03', '$2y$10$Ll6ta.3nEkDARX5ZgwQtY.DwRGJ/dX3b6zLoU/oe0GUo5EKZ5ZYSC', '765pxd5JxvOtKYF4ZNTHKJB7e4heohp1FIuHUJ2m4fiq4rrSegx1lPYE4Cvx', '2020-01-16 14:57:03', '2020-01-16 14:57:03'),
(2, NULL, 'Ida Gusikowski', 'customer@customer', '2020-01-16 14:57:04', '$2y$10$y8XLUlwtgXdFqsf9uzucMuDI6iV3.YzxgJyteRiJEf5rcOgiTUr6u', 'RCdMsBYm9y', '2020-01-16 14:57:04', '2020-01-16 14:57:04');

-- --------------------------------------------------------

--
-- Table structure for table `wisatas`
--

CREATE TABLE `wisatas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `idJenis` int(11) NOT NULL,
  `foto` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wisatas`
--

INSERT INTO `wisatas` (`id`, `idJenis`, `foto`, `nama`, `harga`, `alamat`, `rating`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 1, 'papuma.jpeg', 'Papuma', '15000', 'Unnamed Road, Wuluhan, 68172, Area Kebun, Lojejer, Wuluhan, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 'watuUlo.jpg', 'Watu Ulo', '7500', 'Watu Ulo, Sumberrejo, Ambulu, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 'nanggelan.jpg', 'Nanggelan', '5000', 'Jalan Cangak Indah, Curahnongko, Tempurejo, Area Hutan, Curahnongko, Tempurejo, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, 'puger.jpg', 'Puger', '5000', 'Jalan Pelabuhan, Puger Wetan, Puger, Jember, Mandaran, Puger Wetan, Puger, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 'payangan.jpg', 'Payangan', '5000', 'Jl. Sidomulyo, Sido Mulyo, Sumberrejo, Ambulu, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 1, 'bandealit.jpg', 'Bandealit', '5000', 'Area Hutan, Andongrejo, Tempurejo, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 'paseban.png', 'Paseban', '5000', 'Paseban, Kencong, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 1, 'pancer.jpg', 'Pancer', '5000', 'Manderan II, Puger Kulon, Puger, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 2, 'tancakTulis.JPG', 'Tancak Tulis', '0', 'Area Hutan, Darungan, Tanggul, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 2, 'rowosari.jpg', 'Rowosari(Tuju bidadari) ', '5000', 'Gardu Utara, Rowosari, Sumberjambe, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 2, 'sumberSalak.jpg', 'Sumbersalak', '5000', 'Salak, Sumber Salak, Ledokombo, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 2, 'tancak.jpg', 'Tancak', '0', 'Area Pegunungan Argop, Suci, Panti, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 2, 'anugrah.jpg', 'Anugrah', '0', 'Gendir, Klungkung, Sukorambi, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 2, 'antrokan.jpeg', 'Antrokan', '5000', 'Kali Tengah, Manggisan, Tanggul, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 2, 'sumberlesung.jpg', 'Antrokan Sumberlesung', '0', 'Onjur, Sumber Lesung, Ledokombo, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 3, 'botani.jpg', 'Taman Botani', '20000', 'Jalan Mujahir, Sukorambi, Krajan, Sukorambi, Kec. Sukorambi, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 3, 'gunungPasang.jpg', 'Gunung Pasang', '5000', 'Afdeling Boma Gunung Pasang, Argopuro, Guplek, Suci, Panti, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 3, 'glantangan.png', 'Glantangan', '2000', 'Area Sawah/Kebun, Pondokrejo, Tempurejo, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 3, 'mumbulGarden.jpg', 'Mumbul Garden ', '5000', 'Lengkong, Mumbulsari, Krajan, Lengkong, Mumbulsari, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 3, 'puslit.jpg', 'Agrowisata Puslit', '5000', 'Jl. PB. Sudirman No. 90, Nogosari, Rambipuji, Gebang, Nogosari, Kec. Rambipuji, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 3, 'rembangan.jpg', 'Rembangan', '7500', 'Darungan, Kemuninglor, Arjasa, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 3, 'gunungGambir.jpg', 'Gunung Gambir', '5000', 'Lanasan, Gelang, Sumberbaru, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 3, 'galaxy.jpg', 'Galaxi', '1000', 'Area Sawah/Kebun, Tempurejo, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 3, 'j88.jpg', 'J88', '5000', 'Unnamed Road, Cangkreng, Sucopangepok, Jelbuk, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 4, 'patemon.jpg', 'Patemon', '5000', 'Krajan II, Patemon, Tanggul, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 4, 'dira.jpg', 'Dira Park', '15000', 'Jl. Kota Blater No.KM.4, Watukebo, Andongsari, Ambulu, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 4, 'niagara.jpg', 'Niagara Waterpark', '20000', '-8.3460990 113.6228790, Langon, Ambulu, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 4, 'tiara.jpg', 'Tiara Waterpark', '25000', 'Jl. Kaliurang, Krajan Barat, Sumbersari, Kec. Sumbersari, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 4, 'kolamRembangan.jpg', 'Rembangan', '7500', 'Darungan, Kemuninglor, Arjasa, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 4, 'kebonAgung.jpg', 'Kebonagung', '7000', 'Gebang Taman, Kebon Agung, Kaliwates, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 4, 'kimo.jpg', 'Kimo', '12500', 'Jl. Pangeran Limboro, Sumber Salak, Kranjingan, Kec. Sumbersari, Kabupaten Jember', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 4, 'oleng.png', 'Oleng sibutong', '7000', 'Wisata Oleng sibutong, Binting Pinggir, Biting, Arjasa, Kabupaten Jember', 2, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 4, 'greenHill.jpg', 'GreenHill', '15000', 'Area Sawah/Kulon, Baratan, Patrang, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 5, 'duplang.jpg', 'Duplang ', '2000', 'Duplang, Kamal, Arjasa, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 5, 'beteng.jpg', 'Beteng', '2000', 'Beteng, Sidomekar, Semboro, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 6, 'tubing.jpg', 'Adventur tubing & Rafting', '20000', 'Mrapen, Sumber Kejayan, Mayang, Jember Regency', 3, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 7, 'makamHabib.jpg', 'Habib Sholeh Tanggul', '5000', 'Krajan, Tanggul Kulon, Tanggul, Jember Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(38, 7, 'shiddiq.jpg', 'K.H. Siddiq', '2000', 'Kec. Kaliwates, Kabupaten Jember,Jawa Timur', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 7, 'muchlisin.jpg', 'Roudhotul Mukhlisin', '0', 'Jl. Gajah Mada No.180, Kaliwates Kidul, Kaliwates, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 7, 'cengHo.png', 'Masjid Muhammad Cengho', '3000', 'Jalan Hayam Wuruk No.73, Sempusari, Kaliwates, Gerdu, Sempusari, Kec. Kaliwates, Kabupaten Jember', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(41, 8, 'gumitir.jpg', 'Gunung Gumitir', '2000', 'Dusun Barurejo, Kalibaru Manis, Kalibaru, Banyuwangi Regency', 4, 'a', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotels`
--
ALTER TABLE `hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_wisatas`
--
ALTER TABLE `jenis_wisatas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kamar_hotels`
--
ALTER TABLE `kamar_hotels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriterias`
--
ALTER TABLE `kriterias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kuliners`
--
ALTER TABLE `kuliners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasis`
--
ALTER TABLE `lokasis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ranges`
--
ALTER TABLE `ranges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `wisatas`
--
ALTER TABLE `wisatas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hotels`
--
ALTER TABLE `hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `jenis_wisatas`
--
ALTER TABLE `jenis_wisatas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kamar_hotels`
--
ALTER TABLE `kamar_hotels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `kriterias`
--
ALTER TABLE `kriterias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kuliners`
--
ALTER TABLE `kuliners`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `lokasis`
--
ALTER TABLE `lokasis`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ranges`
--
ALTER TABLE `ranges`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wisatas`
--
ALTER TABLE `wisatas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
