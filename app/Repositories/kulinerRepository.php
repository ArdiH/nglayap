<?php
namespace App\Repositories;

use App\Models\Kuliner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class kulinerRepository{
    private $model;

    public function __construct(Kuliner $model){
        $this->model = $model;
    }

    public function get($pagination = null, $with = null){
        $kuliner = $this->model
            ->when($with, function($query) use ($with){
                return $query->with($with);
            });
        
        if($pagination){
            return $kuliner->paginate($pagination);
        }

        return $kuliner->get();
    }

    public function store(Request $request){
        DB::beginTransaction();
        $detail = new Kuliner();

        try {
            if($request->file('foto')!=null){
                if($detail->foto !=null){
                    $imagepath=public_path().'/assets/img/kuliner/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/kuliner/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $kuliner = Kuliner::create([
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'alamat' => $request->get('alamat'),
                'rating' => $request->get('rating'),
                'deskripsi' => $request->get('deskripsi'),
            ]);
         
            DB::commit();
            return $kuliner;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function update(Request $request, Kuliner $kuliner, $id){
        DB::beginTransaction();
        $detail = Kuliner::find($id);

        try {
            if($request->file('foto')!=null){
                if($detail->foto !=null){
                    $imagepath=public_path().'/assets/img/kuliner/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/kuliner/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $kuliner->where('id',$id)->update([
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'alamat' => $request->get('alamat'),
                'rating' => $request->get('rating'),
                'deskripsi' => $request->get('deskripsi'),
            ]);

            DB::commit(); 
            return $kuliner;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e);
        }
    }
}