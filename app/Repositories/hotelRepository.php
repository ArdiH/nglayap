<?php
namespace App\Repositories;

use App\Models\Hotel;
use App\Models\KamarHotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class hotelRepository{
    private $model;
    private $kamarHotel;

    public function __construct(Hotel $model, KamarHotel $kamarHotel){
        $this->model = $model;
        $this->kamarHotel = $kamarHotel;
    }

    public function get($pagination = null, $with = null){
        $hotel = $this->model
            ->when($with, function($query) use ($with){
                return $query->with($with);
            });

        if($pagination){
            return $hotel->paginate($pagination);
        }

        return $hotel->get(); 
    }

    public function store(Request $request){
        DB::beginTransaction();
        $detail = new Hotel();

        try{
            if($request->file('foto') != null){
                if($detail->foto != null){
                    $imagepath=public_path().'/assets/img/hotel/'.$detail->foto;
                    unlink($imagepath);
                }

                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/hotel/'), $newname);
            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $hotel = Hotel::create([
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'bintangHotel' => $request->get('bintangHotel'),
                'alamat' => $request->get('alamat'),
                'rating' => $request->get('rating'),
                'deskripsi' => $request->get('deskripsi'),
            ]);

            DB::commit();
            return $hotel;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function update(Request $request, Hotel $hotel, $id){
        DB::beginTransaction();
        $detail = Hotel::find($id);

        try {
            if($request->file('foto')!=null){
                if($detail->foto !=null){
                    $imagepath=public_path().'/assets/img/hotel/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/hotel/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $hotel->where('id',$id)->update([
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'bintangHotel' => $request->get('bintangHotel'),
                'alamat' => $request->get('alamat'),
                'rating' => $request->get('rating'),
                'deskripsi' => $request->get('deskripsi'),
            ]);

            DB::commit(); 
            return $hotel;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function getKamar($pagination = null, $with = null){
        $kamar = $this->kamarHotel
            ->when($with, function($query) use ($with){
                return $query->with($with);
            });
        
            if($pagination){
                return $kamar->paginate($pagination);
            }

        return $kamar->get();
    }

    public function createKamar($id){
        $create = Hotel::find($id);
        return $create;
    }

    public function storeKamar(Request $request){
        DB::beginTransaction();
        $detail = new KamarHotel();

        try{
            if($request->file('foto') != null){
                if($detail->foto != null){
                    $imagepath=public_path().'/assets/img/kamarHotel/'.$detail->foto;
                    unlink($imagepath);
                }

                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/kamarHotel/'), $newname);
            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $kamar = KamarHotel::create([
                'idHotel' => $request->get('idHotel'), 
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'fasilitas' => $request->get('fasilitas'),
                'deskripsi' => $request->get('deskripsi'),
            ]);

            DB::commit();
            return $kamar;

        }catch(\Exception $e){
            DB::rollBack();
            throw new \Exception($e);
        }
    }

    public function editKamar($id){
        $edit = KamarHotel::find($id);
        return $edit;
    }

    public function updateKamar(Request $request, KamarHotel $kamar, $id){
        DB::beginTransaction();
        $detail = KamarHotel::find($id);

        try {
            if($request->file('foto')!=null){
                if($detail->foto !=null){
                    $imagepath=public_path().'/assets/img/kamarHotel/'.$detail->foto;
                    unlink($imagepath);
                }
                $image = $request->file('foto');
                $imageName = $image->getClientOriginalName();
                $fileName = pathinfo($imageName, PATHINFO_FILENAME);
                $ext = $request->file('foto')->getClientOriginalExtension();
                $tgl = Carbon::now()->format('dmYHis');
                $newname = $fileName . $tgl . "." . $ext;
                $image->move(public_path('assets/img/kamarHotel/'), $newname); 

            }else if($detail->foto!=null){
                $newname = $detail->foto;
            }else{
                $imageName = null;
                $newname = null;
            }

            $kamar->where('id',$id)->update([
                'idHotel' => $request->get('idHotel'), 
                'foto' => $newname,
                'nama' => $request->get('nama'), 
                'harga' => $request->get('harga'),
                'fasilitas' => $request->get('fasilitas'),
                'deskripsi' => $request->get('deskripsi'),
            ]);

            DB::commit(); 
            return $kamar;
        } catch (\Exception $e) {
            DB::rollBack();
            throw new \Exception($e);
        }
    }
}
