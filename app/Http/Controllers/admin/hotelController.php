<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\KamarHotel;
use App\Models\DataPrediksi;
use App\Repositories\hotelRepository;
use App\Http\Requests\HotelStoreRequest;
use App\Http\Requests\HotelUpdateRequest;
use App\Http\Requests\KamarStoreRequest;
use App\Http\Requests\KamarUpdateRequest;
use Redirect;

class hotelController extends Controller{

    private $hotelRepository;

    public function __construct(hotelRepository $hotelRepository){
        return $this->hotelRepository = $hotelRepository;
    }

    public function index(){
        $hotel = $this->hotelRepository->get();
        return view('admin.hotel.listHotel.index', compact('hotel'));
    }

    public function create(){
        return view('admin.hotel.listHotel.create');
    }

    public function store(HotelStoreRequest $request){
        $this->hotelRepository->store($request);
        return redirect(route('hotel.index'))->with('status', 'Data Hotel Berhasil Ditambahkan');
    }

    public function edit($id){
        $edit = Hotel::find($id);
        return view('admin.hotel.listHotel.edit', compact('edit'));   
    }

    public function update(HotelUpdateRequest $request, Hotel $hotel, $id){
        $this->hotelRepository->update($request, $hotel, $id);
        return redirect(route('hotel.index'))->with('status', 'Data Hotel Berhasi Dirubah');
    }

    public function destroy($id){
        $hotel = Hotel::where('id', $id)->first();
        $prediksi = DataPrediksi::where('idLokasi', '=', $id)->get();
        $kamarHotel = KamarHotel::where('idHotel', '=', $id)->get();
        if($kamarHotel->count() > 0){
            return Redirect::back()->with('danger', 'Data Kamar Hotel Tidak Kosong, Harap Hapus Terlebih Dahulu');

        }elseif($kamarHotel->count() == 0){
            $hotel->forceDelete();
            return redirect(route('hotel.index'))->with('status', 'Data Hotel Berhasil Dihapus');

        }elseif($prediksi->count() > 0){
            return Redirect::back()->with('danger', 'Data Prediksi Tidak Kosong, Harap Hapus Terlebih Dahulu');

        }elseif($prediksi->count() == 0){
            $hotel->forceDelete();
            return redirect(route('hotel.index'))->with('status', 'Data Hotel Berhasil Dihapus');
        }
    }

    public function indexKamar(Request $request){
        $kamar = $this->hotelRepository->getKamar();
        $page = $request->get('page', 1);
        $limit = 5;
        $hotel = $this->hotelRepository->get($limit);
        return view('admin.hotel.kamarHotel.index', compact('hotel', 'kamar'));
    }

    public function editKamar($id){
        $edit = $this->hotelRepository->editKamar($id);
        return view('admin.hotel.kamarHotel.edit', compact('edit'));
    }

    public function updateKamar(KamarUpdateRequest $request, KamarHotel $kamar, $id){
        $this->hotelRepository->updateKamar($request, $kamar, $id);
        return redirect(route('hotel.indexKamar'))->with('status', 'Data Kamar Hotel Berhasi Dirubah');
    }

    public function createKamar($id){
        $create = $this->hotelRepository->createKamar($id);
        return view('admin.hotel.kamarHotel.create', compact('create'));
    }

    public function storeKamar(KamarStoreRequest $request){
        $this->hotelRepository->storeKamar($request);
        return redirect(route('hotel.indexKamar'))->with('status', 'Data Kamar Berhasil Ditambahkan');
    }

    public function destroyKamar($id){
        $kamar = KamarHotel::where('id', $id)->first();
        $kamar->forceDelete();
        return redirect(route('hotel.indexKamar'))->with('status', 'Data Kamar Hotel Berhasil Dihapus');
    }
}
