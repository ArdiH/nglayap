<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\kulinerRepository;
use App\Models\Kuliner;
use App\Http\Requests\KulinerStoreRequest;
use App\Http\Requests\KulinerUpdateRequest;
use App\Models\DataPrediksi;
use Redirect;

class kulinerController extends Controller{

    private $kulinerRepository;

    public function __construct(kulinerRepository $kulinerRepository){
        $this->kulinerRepository = $kulinerRepository;
    }

    public function index(){
        $kuliner = $this->kulinerRepository->get();
        return view('admin.kuliner.index', compact('kuliner'));
    }

    public function create(){
        return view('admin.kuliner.create');
    }

    public function store(KulinerStoreRequest $request){
        $this->kulinerRepository->store($request);
        return redirect(route('kuliner.index'))->with('status', 'Data Kuliner Berhasil Ditambahkan');
    }

    public function edit($id){
        $edit = Kuliner::find($id);
        return view('admin.kuliner.edit', compact('edit'));
    }

    public function update(KulinerUpdateRequest $request, Kuliner $kuliner, $id){
        $this->kulinerRepository->update($request, $kuliner, $id);
        return redirect(route('kuliner.index'))->with('status', 'Data Kuliner Berhasil Dirubah');
    }

    public function destroy($id){
        $kuliner = Kuliner::where('id', $id)->first();
        $prediksi = DataPrediksi::where('idLokasi', '=', $id)->get();
        if($prediksi->count() > 0){
            return Redirect::back()->with('danger', 'Data Prediksi Tidak Kosong, Harap Hapus Terlebih Dahulu');
        }elseif($prediksi->count() == 0){
            $kuliner->forceDelete();
            return redirect(route('kuliner.index'))->with('status', 'Data Kuliner Berhasil Dihapus');
        }
    }
}
