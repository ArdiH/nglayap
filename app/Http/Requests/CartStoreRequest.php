<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartStoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }


    public function rules(){
        return [
            'idUser' => 'required|integer',
            'idKategoriData' => 'required|integer',
            'idJenisData' => 'required|integer',
        ];
    }
}
