<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KriteriaStoreRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'nama' => 'required|max:191',
            'kategori' => 'required|max:191',
            'satuan' => 'required|max:191',
        ];
    }
}
