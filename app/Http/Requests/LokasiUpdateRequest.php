<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LokasiUpdateRequest extends FormRequest{

    public function authorize(){
        return true;
    }

    public function rules(){
        return [
            'nama' => 'required|max:191',
            'alamat' => 'required|max:191',
        ];
    }
}
