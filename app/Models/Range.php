<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Range extends Model{
    protected $fillable = [
        'idKriteria','nama', 'rentang', 'bobot',
    ];

    public function kriteria(){
        return $this->belongsTo('App\Models\Kriteria', 'idKriteria','id');
    }
}
