<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wisata extends Model{
    protected $fillable = [
        'idJenis', 'foto', 'nama', 'harga', 'alamat' , 'rating', 'deskripsi',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function dataPrediksi(){
        return $this->hasMany('App\Models\DataPrediksi', 'id', 'idJenisData');
    }

    public function cart(){
        return $this->hasMany('App\Models\Cart', 'id', 'idJenisData');
    }
}
