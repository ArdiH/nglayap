<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model{
    public $table = 'user_profile';

    protected $fillable = [
        'id_user', 'postcode', 'id_regency', 'alamat', 'phone',
    ];

    public function userRegencies(){
        return $this->belongsTo('App\Models\Regencies', 'id_regency', 'id');
    }
    
}
