<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KamarHotel extends Model{
    protected $fillable = [
        'idHotel', 'foto', 'nama', 'harga', 'fasilitas', 'deskripsi',
    ];

    public $timestamps = false;
}
