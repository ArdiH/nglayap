<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Kriteria extends Migration{

    public function up(){
        Schema::create('kriterias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('kategori');
            $table->string('satuan')->nullable();
        });
    }

    public function down(){
        Schema::dropIfExists('kriterias');
    }
}
