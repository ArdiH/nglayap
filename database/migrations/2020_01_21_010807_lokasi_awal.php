<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LokasiAwal extends Migration{

    public function up(){
        Schema::create('lokasis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama');
            $table->string('alamat');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('lokasis');
    }
}
