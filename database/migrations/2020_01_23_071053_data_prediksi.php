<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DataPrediksi extends Migration{

    public function up(){
        Schema::create('data_prediksis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idLokasi');
            $table->integer('idKategoriData');
            $table->integer('idJenisData');
            $table->double('jarak');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('data_prediksis');
    }
}
