<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserProfile extends Migration{

    public function up(){
        Schema::create('user_profile', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_user');
            $table->string('postcode');
            $table->integer('id_province');
            $table->integer('id_regency');
            $table->string('alamat');
            $table->string('phone');
            $table->timestamps();
        });
    }

    public function down(){
        Schema::dropIfExists('user_profile');
    }
}
