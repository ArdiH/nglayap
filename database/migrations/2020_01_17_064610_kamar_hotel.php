<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class KamarHotel extends Migration{

    public function up(){
        Schema::create('kamar_hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('idHotel');
            $table->string('foto');
            $table->string('nama');
            $table->integer('harga');
            $table->string('fasilitas');
            $table->string('deskripsi');
        });
    }

    public function down(){
        Schema::dropIfExists('kamar_hotels');
    }
}
